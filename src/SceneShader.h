/*
 * SceneShader.h
 *
 */

#ifndef SCENESHADER_H_
#define SCENESHADER_H_

#include "Shader.h"
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include "utils/trackball.hpp"

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "texture.h"
#include <memory>
#include "framebuffer.h"
#include "edgedetectionrender.h"
#include "Grid.h"
#include "simplequadrender.h"

class SceneShader : public Shader
{
public:

	SceneShader( shared_ptr<Tucano::Trackball> camera );
	~SceneShader();

	void startup ();
	void shutdown ();
	void render();

	void renderSample(SamplePoint *PS, glm::vec3 color);
	void renderBBoxGeometry(SamplePoint *PB);
	void renderBBox(SamplePoint PB, int index);
	void renderQuad(SamplePoint PB);
	void renderLight();

	void renderGlyphGeometry(SamplePoint *PB, glm::vec3 color);
	void renderGlyph(SamplePoint PB, int index);
	void renderGlyphTex(SamplePoint *PB);

	void SetParameters(int H,int W,int D, int c, int b, SamplePoint SPP[6][1000000], float *dataP);
	void printInfo();

	void GridToggle(int s);
	void GlyphToggle(int s);
	void LevelToggle(int s);
	void TexToggle(int s);
	void ArrowSize(int s);

private:

	/*methods*/

	void createVertexBuffer();

	/*variables*/

	std::vector<unsigned char> _image[15];
	unsigned int _imageWidth;
	unsigned int _imageHeight;

	GLuint _programAxis;
	GLuint _programLight;
	GLuint _programGlyph;
	GLuint _programMesh;

	GLuint _axisVertexArray;
	GLuint _axisVertexBuffer;
	GLuint _axisDirectionArray;
	GLuint _axisDirectionBuffer;
	GLuint _axisTextureBuffer;

	GLuint _glyphVertexArray;
	GLuint _glyphVertexBuffer;
	GLuint _glyphDirectionArray;
	GLuint _glyphDirectionBuffer;
	GLuint _glyphTextureBuffer;

	GLuint _meshVertexArray;
	GLuint _meshVertexBuffer;
	GLuint _meshNormalBuffer;
	GLuint _meshIndicesBuffer;
	GLuint _meshTextureBuffer;

	GLint _mvUniform, _projUniform;

	/* Matrices */
	glm::mat4 _modelview;
	glm::mat4 _projection;

	float _zTranslation;
	float _xRot;
	float _yRot;
	float _zRot;
	float _aspectRatio;
	float _scale;
	float radius;
	float GridFlag;
	float GlyphFlag;
	float LevelFlag;
	float TexFlag;
	float arrowSize;
	int border;
	float *dataV;

	/* Textures */
	Texture _texture;
	Texture _CMtexture;
	Texture _VOLtexture;

	GLuint _textureImageID[15];
	GLuint _CMtextureImageID;
	GLuint _VOLtextureImageID;

	/* vertices */
	glm::vec3 lightPosition;
	glm::vec3 translate;


	std::shared_ptr<Tucano::Trackball> _camera;
	std::shared_ptr<FrameBuffer> _fbo;
	std::shared_ptr<EdgeDetectionRender> _edgeDetectionRender;

	float count = 1;

    std::vector<GLubyte> _greenSequentialColormap;
    std::vector<GLubyte> _redSequentialColormap;

    GLuint _redColormapID;
    GLuint _greenColormapID;

    SamplePoint SPR[6][1000000];

    std::shared_ptr<SimpleQuadRender> _quadRender;

};

#endif /* SCENESHADER_H_ */
