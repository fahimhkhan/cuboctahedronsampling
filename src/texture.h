#ifndef TEXTURE_H
#define TEXTURE_H

#include "GL/glew.h"
#include <vector>
#include <string>

class Texture
{
public:
    Texture();

	 GLuint create1DTexture(std::vector<GLubyte>& rgbaValues);
	 GLuint create2DTexture(std::vector<unsigned char>& image, unsigned int width, unsigned int height);
	 //GLuint create3DTexture(std::vector<unsigned char>& image, unsigned int width, unsigned int height, unsigned int depth);
	 GLuint create3DTexture(float* image, unsigned int width, unsigned int height, unsigned int depth);
	 
	 void bind1DTexture(GLuint _program, GLuint _textureID, std::string varName);
	 void bind2DTexture(GLuint _program, GLuint _textureID, std::string varName);
	 void bind3DTexture(GLuint _program, GLuint _textureID, std::string varName);
	 
	 void unbind1DTexture();
	 void unbind2DTexture();
	 void unbind3DTexture();

};

#endif // TEXTURE_H
