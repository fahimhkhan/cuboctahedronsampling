#include "edgedetectionrender.h"

EdgeDetectionRender::EdgeDetectionRender( )
{
}


void EdgeDetectionRender::startup()
{
   std::string vertexShader = std::string("./shaders/edgeDetection.vert");
   std::string fragmentShader = std::string("./shaders/edgeDetection.frag");

   _program = compile_shaders(vertexShader.c_str(), fragmentShader.c_str());

   createVertexBuffer();
}


EdgeDetectionRender::~EdgeDetectionRender()
{
	shutdown();
}



void EdgeDetectionRender::createVertexBuffer()
{
    glm::vec2 v0(-1.0, -1.0);
    glm::vec2 v1(-1.0, 1.0);
    glm::vec2 v2(1.0, -1.0);
    glm::vec2 v3(1.0, 1.0);

    std::vector<glm::vec4> vertices;

    vertices.push_back(glm::vec4(v0.x, v0.y, 0.0f, 0.0f));
    vertices.push_back(glm::vec4(v1.x, v1.y, 0.0f, 1.0f));
    vertices.push_back(glm::vec4(v2.x, v2.y, 1.0f, 0.0f));
    vertices.push_back(glm::vec4(v3.x, v3.y, 1.0f, 1.0f));



    glGenVertexArrays(1, &_vertexArray);
    glBindVertexArray(_vertexArray);

    glGenBuffers(1, &_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec4), vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void EdgeDetectionRender::shutdown()
{
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArrays(1, &_vertexArray);

    glDeleteProgram(_program);
}


void EdgeDetectionRender::setTexture( GLuint texture )
{
	_textureID = texture;
}


void EdgeDetectionRender::render()
{
    glBindVertexArray(_vertexArray);
    glUseProgram(_program);

    glActiveTexture(GL_TEXTURE0 + _textureID);
    glBindTexture(GL_TEXTURE_2D, _textureID);
    glUniform1i(glGetUniformLocation(_program, "meshTexture"), _textureID);

    glDrawArrays( GL_TRIANGLE_STRIP, 0, 4);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindVertexArray(0);
}

