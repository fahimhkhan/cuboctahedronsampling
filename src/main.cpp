//============================================================================
// Name        : Sampling101.cpp
// Author      :
// Version     :
// Copyright   : Copyright notice
// Description : Sampling
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <random>
#include "GL/glew.h"
#include <cstdlib>
#include <cstdio>
#include <GLFW/glfw3.h>
#include <memory>

#include "SceneShader.h"
#include "Grid.h"

using namespace std;

std::mt19937 Mersenne;
std::uniform_real_distribution<float> distribution1(0.0, 1.0);

/* Global Constants */
float V0 = 0.0, V0min = 99999999.0, V0max = 0.0, V1 = 0.0, V1min = 99999999.0, V1max = 0.0;
int GridCounter = 0;

int HL = 4;
float Threshold = 0.0005;
//float Threshold = 0.5;

//Vortex Ring Data
int imageH = 17;
int imageW = 17;
int imageD = 17;
int imageT = 3;
std::string path = "/media/fahimhkhan/Data/VectorField2/VR";

//Aneurysm Data
//int imageH = 61;
//int imageW = 41;
//int imageD = 61;
//int imageT = 3;
//std::string path = "/media/fahimhkhan/Data/AneVfs/MRI";

//Number of grid cells in x,y,z direction
int nx = imageH-1;
int ny = imageW-1;
int nz = imageD-1;

int bx = nx/BLOCK_SIZE;
int by = ny/BLOCK_SIZE;
int bz = nz/BLOCK_SIZE;

SamplePoint SP[6][1000000];
int s_count = 0;
int border = 0;

double mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = 0.0, rotate_y = 0.0, rotate_z = 0.0;

int width = 1280;
int height = 1024;
GLFWwindow* window;

shared_ptr<SceneShader> scene;
shared_ptr<Tucano::Trackball> camera;

const int size = imageH*imageW*imageD;

float **data_x = new float*[6];
float **data_y = new float*[6];
float **data_z = new float*[6];

float **data_V = new float*[6];

SamplePoint trilinearC(GridCell GT, int L){

	float Wt=(1-0.5)*(1-0.5)*(1-0.5);
    SamplePoint ipC;

	for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				for (int k=0;k<2;k++){
				ipC.u += Wt*GT.P[i][j][k].u;
				ipC.v += Wt*GT.P[i][j][k].v;
				ipC.w += Wt*GT.P[i][j][k].w;
				}
	ipC.x = (GT.P[0][0][0].x + GT.P[1][1][1].x)/2.0;
	ipC.y = (GT.P[0][0][0].y + GT.P[1][1][1].y)/2.0;
	ipC.z = (GT.P[0][0][0].z + GT.P[1][1][1].z)/2.0;

	ipC.hLevel = L;

	return ipC;
}

SamplePoint interpolateM(SamplePoint P1, SamplePoint P2, int L){

    SamplePoint iP;

    float x = (P1.x + P2.x)/2.0;
    float y = (P1.y + P2.y)/2.0;
    float z = (P1.z + P2.z)/2.0;
    float u = (P1.u + P2.u)/2.0;
    float v = (P1.v + P2.v)/2.0;
    float w = (P1.w + P2.w)/2.0;

    iP.setpoints(x,y,z,u,v,w,L);

    return iP;
}

float BlockVariance(int x, int y, int z, float* uVol, float* vVol, float* wVol){
	int size = BLOCK_SIZE*BLOCK_SIZE*BLOCK_SIZE;
	SamplePoint BP[size], sum, sum1, Vavg, Vvar;
	float Bvar=0.0;
	int count = 0;

	for (int i=0;i<BLOCK_SIZE;i++)
			for (int j=0;j<BLOCK_SIZE;j++)
				for (int k=0;k<BLOCK_SIZE;k++){
					float u = uVol[imageH*imageW*(k+z)+imageW*(i+x)+(j+y)];
					float v = vVol[imageH*imageW*(k+z)+imageW*(i+x)+(j+y)];
					float w = wVol[imageH*imageW*(k+z)+imageW*(i+x)+(j+y)];
					BP[count].setpoints((x+i),(y+j),(z+k),u,v,w,0);
					count++;
				}

	for(int i = 0; i < size; ++i){
					    sum.u += BP[i].u;
					    sum.v += BP[i].v;
					    sum.w += BP[i].w;
				}

					Vavg.u = sum.u / size;
					Vavg.v = sum.v / size;
					Vavg.w = sum.w / size;

					for(int i = 0; i < size; ++i){
						float diff= 0.0;
						diff = BP[i].u - Vavg.u;
					    sum1.u += (diff*diff);
						diff = BP[i].v - Vavg.v;
					    sum1.v += (diff*diff);
						diff = BP[i].w - Vavg.w;
					    sum1.w += (diff*diff);
					}

					Vvar.u = (sum1.u / size);
					Vvar.v = (sum1.v / size);
					Vvar.w = (sum1.w / size);

					Bvar = (Vvar.u + Vvar.v + Vvar.w)/3;
					//printf("\nBLock Analysis Var= %f Count= %d", Bvar, count);
					return Bvar;
}

float varianceCalc1(GridCell GC5){
	SamplePoint A[8], sum, sum1, Vavg, Vvar;
	float Avar=0.0;
	int count=0;

	//GC5.P[1][1][1].print2();

	for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				for (int k=0;k<2;k++){
					A[count].setpoints(GC5.P[i][j][k].x,GC5.P[i][j][k].y,GC5.P[i][j][k].z,GC5.P[i][j][k].u,GC5.P[i][j][k].v,GC5.P[i][j][k].w,GC5.P[i][j][k].hLevel);
					count++;
				}

	for(int i = 0; i < 8; ++i){
	    sum.u += A[i].u;
	    sum.v += A[i].v;
	    sum.w += A[i].w;
	}

	Vavg.u = sum.u / 8;
	Vavg.v = sum.v / 8;
	Vavg.w = sum.w / 8;

	for(int i = 0; i < 8; ++i){
		float diff= 0.0;
		diff = A[i].u - Vavg.u;
	    sum1.u += (diff*diff);
		diff = A[i].v - Vavg.v;
	    sum1.v += (diff*diff);
		diff = A[i].w - Vavg.w;
	    sum1.w += (diff*diff);
	}

	Vvar.u = (sum1.u / 8);
	Vvar.v = (sum1.v / 8);
	Vvar.w = (sum1.w / 8);

	Avar = (Vvar.u + Vvar.v + Vvar.w)/3;

	return Avar;
}

SamplePoint Cuboctahedron(SamplePoint C){
	SamplePoint SC=C;

	glm::vec3 VC, V[12], V1, midpoint, midpoint2;
	float d[12], dmax =-0.0; //decision variable

	VC = glm::vec3(C.u ,C.v , C.w);

	midpoint2 = ((C.box[1][1][0])+(C.box[1][1][1]));
	midpoint = glm::vec3((midpoint2.x/2.0),(midpoint2.y/2.0),(midpoint2.z/2.0));
	V[0] = glm::vec3((midpoint.x-C.x), (midpoint.y-C.y), (midpoint.z-C.z) );

	midpoint2 = ((C.box[1][0][0])+(C.box[1][0][1]));
	midpoint = glm::vec3((midpoint2.x/2.0),(midpoint2.y/2.0),(midpoint2.z/2.0));
	V[1] = glm::vec3((midpoint.x-C.x), (midpoint.y-C.y), (midpoint.z-C.z) );

	V[2] = -V[1];
	V[3] = -V[0];

	midpoint2 = ((C.box[1][1][1])+(C.box[1][0][1]));
	midpoint = glm::vec3((midpoint2.x/2.0),(midpoint2.y/2.0),(midpoint2.z/2.0));
	V[4] = glm::vec3((midpoint.x-C.x), (midpoint.y-C.y), (midpoint.z-C.z) );

	midpoint2 = ((C.box[1][1][0])+(C.box[1][0][0]));
	midpoint = glm::vec3((midpoint2.x/2.0),(midpoint2.y/2.0),(midpoint2.z/2.0));
	V[5] = glm::vec3((midpoint.x-C.x), (midpoint.y-C.y), (midpoint.z-C.z) );

	V[6] = -V[5];
	V[7] = -V[4];

	midpoint2 = ((C.box[1][1][0])+(C.box[1][1][1]));
	midpoint = glm::vec3((midpoint2.x/2.0),(midpoint2.y/2.0),(midpoint2.z/2.0));
	V[8] = glm::vec3((midpoint.x-C.x), (midpoint.y-C.y), (midpoint.z-C.z) );

	midpoint2 = ((C.box[0][1][0])+(C.box[1][1][0]));
	midpoint = glm::vec3((midpoint2.x/2.0),(midpoint2.y/2.0),(midpoint2.z/2.0));
	V[9] = glm::vec3((midpoint.x-C.x), (midpoint.y-C.y), (midpoint.z-C.z) );

	V[10] = -V[9];
	V[11] = -V[8];

	for(int i=0;i<12;i++){
		d[i] = (glm::dot(VC, V[i]))/((glm::length(VC))*(glm::length(V[i])));
		if (d[i]>dmax){
			dmax=d[i];
			V1 = V[i];
		}

	}

	float distR = 0.35*(glm::distance(C.box[1][1][1],C.box[0][0][0]));
	//float distR = 1.0/pow(2,SC.hLevel);

	if(SC.hLevel == 0)
		distR = distR*0.25;

	float random1 = distribution1(Mersenne);
	float random2 = distribution1(Mersenne);
	float random3 = distribution1(Mersenne);

	//float dist = glm::distance(glm::vec3(C.x ,C.y , C.z),glm::vec3(C.box[1][1][1].x ,C.box[1][1][1].y , C.box[1][1][1].z));

	//printf("\n Cos angle values %f, %f, %f, %f, dmax =  %f", d[0],d[1],d[2],d[3],dmax);
	//printf("\n Jitter Vector = %f, %f, %f, distance = %f", V1.x, V1.y, V1.z, dist);
	V1 = distR*random1*V1;


	SC.x = SC.x+V1.x;
	SC.y = SC.y+V1.y;
	SC.z = SC.z+V1.z;

	SC.x = SC.x+random1*distR;
	SC.y = SC.y+random2*distR;
	SC.z = SC.z+random3*distR;

	//printf("\n LevelCF = %f, distance = %f", (1.0/pow(2,SC.hLevel)), dist);

	//printf("\n C(%f, %f, %f) -> Jitter -> SC(%f, %f, %f), (Max = %f, Random = %f)", C.x, C.y, C.z, SC.x, SC.y, SC.z, dist, random1);

	return SC;
}


SamplePoint CellSplit(SamplePoint C){
	SamplePoint SC=C;

	glm::vec3 VC, V[4], V1;
	float d[4], dmax =0.0;
	//Split the cell
	VC = glm::vec3(C.u ,C.v , C.w);
	V[0] = glm::vec3((C.box[1][1][1].x-C.box[0][0][0].x), (C.box[1][1][1].y-C.box[0][0][0].y), (C.box[1][1][1].z-C.box[0][0][0].z) );
	V[1] = glm::vec3((C.box[1][1][0].x-C.box[0][0][1].x), (C.box[1][1][0].y-C.box[0][0][1].y), (C.box[1][1][0].z-C.box[0][0][1].z) );
	V[2] = glm::vec3((C.box[1][0][1].x-C.box[0][1][0].x), (C.box[1][0][1].y-C.box[0][1][0].y), (C.box[1][0][1].z-C.box[0][1][0].z) );
	V[3] = glm::vec3((C.box[0][1][1].x-C.box[1][0][0].x), (C.box[0][1][1].y-C.box[1][0][0].y), (C.box[0][1][1].z-C.box[1][0][0].z) );

	d[0] = (glm::dot(VC, V[0]))/((glm::length(VC))*(glm::length(V[0])));
	d[1] = (glm::dot(VC, V[1]))/((glm::length(VC))*(glm::length(V[1])));
	d[2] = (glm::dot(VC, V[2]))/((glm::length(VC))*(glm::length(V[2])));
	d[3] = (glm::dot(VC, V[3]))/((glm::length(VC))*(glm::length(V[3])));

	for(int i=0;i<4;i++){
		d[i] = sqrt(d[i]*d[i]);
		if (d[i]>dmax){
			dmax=d[i];
			V1 = V[i];
		}
	}

	float dist = C.box[1][1][1].x-C.x;
	float distR = 1.0/pow(2,SC.hLevel);

	if(SC.hLevel == 0)
		distR = distR*0.25;

	float random1 = distribution1(Mersenne);
	//float random2 = distribution1(Mersenne);
	//float random3 = distribution1(Mersenne);

	//float dist = glm::distance(glm::vec3(C.x ,C.y , C.z),glm::vec3(C.box[1][1][1].x ,C.box[1][1][1].y , C.box[1][1][1].z));

	//printf("\n Cos angle values %f, %f, %f, %f, dmax =  %f", d[0],d[1],d[2],d[3],dmax);
	//printf("\n Jitter Vector = %f, %f, %f, distance = %f", V1.x, V1.y, V1.z, dist);
	V1 = distR*random1*V1;

	SC.x = SC.x+V1.x;
	SC.y = SC.y+V1.y;
	SC.z = SC.z+V1.z;

	//SC.x = SC.x+random1*dist;
	//SC.y = SC.y+random2*dist;
	//SC.z = SC.z+random3*dist;

	//printf("\n LevelCF = %f, distance = %f", (1.0/pow(2,SC.hLevel)), dist);

	//printf("\n C(%f, %f, %f) -> Jitter -> SC(%f, %f, %f), (Max = %f, Random = %f)", C.x, C.y, C.z, SC.x, SC.y, SC.z, dist, random1);

	return SC;
}

GridCell BuildGridCell(int x, int y, int z, float* uVol, float* vVol, float* wVol){

	GridCell GC2;

	for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				for (int k=0;k<2;k++){
					float u = uVol[imageH*imageW*(k+z)+imageW*(i+x)+(j+y)];
					float v = vVol[imageH*imageW*(k+z)+imageW*(i+x)+(j+y)];
					float w = wVol[imageH*imageW*(k+z)+imageW*(i+x)+(j+y)];
					GC2.P[i][j][k].setpoints((x+i),(y+j),(z+k),u,v,w,1);
				}
	return GC2;
}

void HierarchicalGrid(GridCell G3, int level, int id){
	GridCounter++;
	if(level<HL){
		float Variance;
		Variance = varianceCalc1(G3);
		V1 += Variance;
		if(V1 > V1max) V1max=V1;
		if(V1 < V1min) V1min=V1;

		//if (level==3){
			//G3.P[0][0][0].print();
			//printf("Current Cell (level = %d, Variance=%f)", level, Variance);
		//}
		if (Variance > Threshold){
			++level;
			//printf(" -> Split to level (%d)",level);

			//New Grid cells
			GridCell GC[2][2][2];

			//Interpolating Original Grid Cell
			SamplePoint SP[3][3][3];

			for (int i=0;i<2;i++)
					for (int j=0;j<2;j++)
						for (int k=0;k<2;k++){
						SP[2*i][2*j][2*k]=G3.P[i][j][k];
						}

			SP[1][1][1] = trilinearC(G3,level);

			SP[0][0][1] = interpolateM(SP[0][0][0],SP[0][0][2],level);

			SP[0][2][1] = interpolateM(SP[0][2][0],SP[0][2][2],level);
			SP[2][0][1] = interpolateM(SP[2][0][0],SP[2][0][2],level);
			SP[2][2][1] = interpolateM(SP[2][2][0],SP[2][2][2],level);

			SP[0][1][0] = interpolateM(SP[0][0][0],SP[0][2][0],level);
			SP[0][1][2] = interpolateM(SP[0][0][2],SP[0][2][2],level);

			SP[2][1][0] = interpolateM(SP[2][0][0],SP[2][2][0],level);
			SP[2][1][2] = interpolateM(SP[2][0][2],SP[2][2][2],level);

			SP[1][0][0] = interpolateM(SP[0][0][0],SP[2][0][0],level);
			SP[1][0][2] = interpolateM(SP[0][0][2],SP[2][0][2],level);

			SP[1][2][0] = interpolateM(SP[0][2][0],SP[2][2][0],level);
			SP[1][2][2] = interpolateM(SP[0][2][2],SP[2][2][2],level);

			SP[0][1][1] = interpolateM(SP[0][1][0],SP[0][1][2],level);
			SP[1][0][1] = interpolateM(SP[1][0][0],SP[1][0][2],level);
			SP[1][1][0] = interpolateM(SP[1][0][0],SP[1][2][0],level);
			SP[1][1][2] = interpolateM(SP[1][0][2],SP[1][2][2],level);
			SP[1][2][1] = interpolateM(SP[1][2][0],SP[1][2][2],level);
			SP[2][1][1] = interpolateM(SP[2][1][0],SP[2][1][2],level);

			for (int i=0;i<2;i++)
					for (int j=0;j<2;j++)
							for (int k=0;k<2;k++){
								GC[i][j][k].P[0][0][0]=SP[0+i][0+j][0+k];
								GC[i][j][k].P[0][0][1]=SP[0+i][0+j][1+k];
								GC[i][j][k].P[0][1][0]=SP[0+i][1+j][0+k];
								GC[i][j][k].P[0][1][1]=SP[0+i][1+j][1+k];
								GC[i][j][k].P[1][0][0]=SP[1+i][0+j][0+k];
								GC[i][j][k].P[1][0][1]=SP[1+i][0+j][1+k];
								GC[i][j][k].P[1][1][0]=SP[1+i][1+j][0+k];
								GC[i][j][k].P[1][1][1]=SP[1+i][1+j][1+k];
							}

			for (int i=0;i<2;i++)
					for (int j=0;j<2;j++)
							for (int k=0;k<2;k++){
								HierarchicalGrid(GC[i][j][k],level,id);
							}

		}
		else{
			SamplePoint SPC = trilinearC(G3,level);
			for (int i=0;i<2;i++)
					for (int j=0;j<2;j++)
							for (int k=0;k<2;k++){
								SPC.box[i][j][k] = glm::vec3(G3.P[i][j][k].x, G3.P[i][j][k].y, G3.P[i][j][k].z);
							}
			SamplePoint SPC1 = Cuboctahedron(SPC);
			//SamplePoint SPC1 = (SPC);
			SP[id][s_count] = SPC1;
			s_count++;

			//if (level==3){
				//printf(" -> Add point-%d to list",s_count);
				//SPC.print2();
			//}
		}
	}
//	else if(level==HL){
//		SamplePoint SPC = trilinearC(G3,level);
//					for (int i=0;i<2;i++)
//							for (int j=0;j<2;j++)
//									for (int k=0;k<2;k++){
//										SPC.box[i][j][k] = glm::vec3(G3.P[i][j][k].x, G3.P[i][j][k].y, G3.P[i][j][k].z);
//									}
//					SamplePoint SPC1 = Cuboctahedron(SPC);
//					//SamplePoint SPC1 = (SPC);
//					SP[id][s_count] = SPC1;
//					s_count++;
//	}
}

void BuildGrid(int xb, int yb, int zb,float* uVol, float* vVol, float* wVol, int id){
	int initlevel=1;
	//for each cell
	/*for (int i=0;i<nx;i++)
			for (int j=0;j<ny;j++)
				for (int k=0;k<nz;k++){*/
	for (int i=xb;i<(xb+BLOCK_SIZE);i++)
			for (int j=yb;j<(yb+BLOCK_SIZE);j++)
				for (int k=zb;k<(zb+BLOCK_SIZE);k++){
					GridCell G1 = BuildGridCell(i,j,k,uVol,vVol,wVol);
					HierarchicalGrid(G1,initlevel, id);
				}
}


void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case (27) :
		exit(EXIT_FAILURE);
	case (GLFW_KEY_X) :
		rotate_y += 1.0f;
		break;
	case (GLFW_KEY_Z) :
		rotate_y -= 1.0f;
		break;
	case (GLFW_KEY_G) :
		scene->GridToggle(1);
		break;
	case (GLFW_KEY_H) :
		scene->GridToggle(0);
		break;
	case (GLFW_KEY_F) :
		scene->GridToggle(2);
		break;
	case (GLFW_KEY_J) :
		scene->GlyphToggle(0);
		break;
	case (GLFW_KEY_K) :
		scene->GlyphToggle(1);
		break;
	case (GLFW_KEY_L) :
		scene->GlyphToggle(2);
		break;
	case (GLFW_KEY_1) :
		scene->LevelToggle(1);
		break;
	case (GLFW_KEY_2) :
		scene->LevelToggle(-1);
		break;
	case (GLFW_KEY_3) :
		scene->TexToggle(1);
		break;
	case (GLFW_KEY_4) :
		scene->TexToggle(-1);
		break;
	case (GLFW_KEY_5) :
		scene->ArrowSize(1);
		break;
	case (GLFW_KEY_6) :
		scene->ArrowSize(-1);
		break;
	default:
		break;
	}
}


void mouse(GLFWwindow* window, int button, int action, int mods)
{


	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		camera->rotateCamera( Eigen::Vector2f (xpos, ypos) );
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		camera->endRotation();
	}

	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		camera->translateCamera(Eigen::Vector2f(xpos, ypos));
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		camera->endTranslation();
	}
}

void motion(GLFWwindow* w, double xpos, double ypos)
{

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		camera->rotateCamera(Eigen::Vector2f(xpos, ypos));
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		camera->translateCamera(Eigen::Vector2f(xpos, ypos));
	}

}

void scroll(GLFWwindow* w, double x, double y)
{

	const int WHEEL_STEP = 120;

	double dy;
	dy = (x - y);

	float pos = dy / float (WHEEL_STEP);

	if( (pos > 0) )
	{
		camera->increaseZoom(1.05);
	}

	else if(pos < 0)
	{
		camera->increaseZoom(1.0/1.05);
	}

}

void draw()
{
	GLfloat color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	//GLfloat color[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat one = 1.0f;

	glClearBufferfv(GL_COLOR, 0, color);
	glClearBufferfv(GL_DEPTH, 0, &one);

	scene->render();
}

void reshape(GLFWwindow* w, int widthX, int heightY)
{
	width = widthX;
	height = heightY;
	glViewport(0, 0, width, height);
	camera->setViewport(Eigen::Vector2f ((float)width, (float)height));
	camera->setPerspectiveMatrix(	camera->getFovy(), (float)width/(float)height, 0.01f, 100.0f);

}

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	keyboard(key, 0, 0);

}

void startGlew()
{
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong */
		fprintf(stderr, "Error: %s \n", glewGetErrorString(err));
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* vendor = glGetString(GL_VENDOR); // vendor name string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	// GLSL version string
	const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major); // get integer (only if gl version > 3)
	glGetIntegerv(GL_MINOR_VERSION, &minor); // get dot integer (only if gl version > 3)
	printf("OpenGL on %s %s\n", vendor, renderer);
	printf("OpenGL version supported %s\n", version);
	printf("GLSL version supported %s\n", glslVersion);
	printf("GL version major, minor: %i.%i\n", major, minor);

	camera = make_shared<Tucano::Trackball>();
	camera->setPerspectiveMatrix(45.0, (float)width/(float)height, 0.01f, 100.0f);
	camera->setRenderFlag(true);
	camera->setViewport(Eigen::Vector2f ((float)width, (float)height));
	camera->updateViewMatrix();

	scene = std::make_shared<SceneShader>( camera );

	scene->SetParameters(imageH, imageW, imageD, s_count, border, SP, data_V[1]);
	//scene->SetParameters(5,5,5);
	scene->startup();

}

void readfiles() {
		/* Reading datafiles*/

		//for (int t = 0; t < imageT; t++){
		for (int t = 0; t < imageT; t++){
			std::string ts = std::to_string(t+1);

			std::string fx = path + "xt" + ts + ".raw";
			const char* filename1 = fx.c_str();
			FILE *pFile1 = fopen(filename1, "rb");
			if (NULL == pFile1) { fprintf(stderr, "Error opening file '%s'\n", filename1); }
			data_x[t] = (float*)malloc(sizeof(float)*size);
			fread(data_x[t], sizeof(float), size, pFile1);
			fclose(pFile1);

			//printf("Read completed: '%s', %lu elements\n", filename1, read1);


			std::string fy = path + "yt" + ts + ".raw";
			const char* filename2 = fy.c_str();
			FILE *pFile2 = fopen(filename2, "rb");
			if (NULL == pFile2) { fprintf(stderr, "Error opening file '%s'\n", filename2); }
			data_y[t] = (float*)malloc(sizeof(float)*size);
			fread(data_y[t], sizeof(float), size, pFile2);
			fclose(pFile2);

			//printf("Read completed: '%s', %lu elements\n", filename2, read2);

			std::string fz = path + "zt" + ts + ".raw";
			const char* filename3 = fz.c_str();
			FILE *pFile3 = fopen(filename3, "rb");
			if (NULL == pFile3) { fprintf(stderr, "Error opening file '%s'\n", filename3); }
			data_z[t] = (float*)malloc(sizeof(float)*size);
			fread(data_z[t], sizeof(float), size, pFile3);
			fclose(pFile3);

			//printf("Read completed: '%s', %lu elements\n", filename3, read3);
			data_V[t] = (float*)malloc(sizeof(float)*size*3);
			for(int i=0;i<size;i++){
				data_V[t][(i*3)]=data_x[t][i];
				data_V[t][(i*3)+1]=data_y[t][i];
				data_V[t][(i*3)+2]=data_z[t][i];
			}
		}

		printf("\n\nFile reading complete\n================================================\n");

}

int sampling(int id){
	printf("\n\nCreating Blocks \n================================================\n");

	//BuildBlocks
	for (int i=0;i<bx;i++)
			for (int j=0;j<by;j++)
				for (int k=0;k<bz;k++){
					//printf("\nBuildGrid()");
					float BV = BlockVariance(i*BLOCK_SIZE,j*BLOCK_SIZE,k*BLOCK_SIZE, data_x[id],data_y[id],data_z[id]);
					if(isnan(BV)) BV=1;
					//printf("\n\nBuilding Grids \n================================================\n");
					if(BV>Threshold)
						BuildGrid(i*BLOCK_SIZE,j*BLOCK_SIZE,k*BLOCK_SIZE, data_x[id],data_y[id],data_z[id], id);
					else{
						//Process blocks
						SamplePoint P0, P1, P2, SP0;
						P1.setpoints((i)*BLOCK_SIZE,(j)*BLOCK_SIZE,(k)*BLOCK_SIZE,0);
						P2.setpoints((i+1)*BLOCK_SIZE,(j+1)*BLOCK_SIZE,(k+1)*BLOCK_SIZE,0);
						P0 = interpolateM(P1, P2, 0);
						for (int p=0;p<2;p++)
							for (int q=0;q<2;q++)
								for (int r=0;r<2;r++){
									P0.box[p][q][r] = glm::vec3((i+p)*BLOCK_SIZE,(j+q)*BLOCK_SIZE,(k+r)*BLOCK_SIZE);
									}
						int l = (int)(imageH*imageW*(P0.z)+imageW*(P0.x)+(P0.y));
						P0.u = data_x[id][l];
						P0.v = data_y[id][l];
						P0.w = data_z[id][l];

						//printf("\nBlock Properties = ");
						//P0.print2();
						//P0.printBBox();

						//for(int m=0;m<1;m++){
							SP0 = Cuboctahedron(P0);
							SP[id][s_count] = SP0;
							s_count++;
						//}
					}
				}
	//BuildGrid(0,0,0);

	printf("\n\nAnalyzing Variance in Grids \n================================================\n");

	//float AvgR1 = V0/GridCounter;
	//printf("\n\nTechnique 1 \n=================\n Variance Avg = %f, Min = %f, Max = %f", AvgR1,V0min,V0max);

	float AvgR2 = V1/GridCounter;
	printf("\n\nTechnique 2 \n=================\n Variance Avg = %f, Min = %f, Max = %f", AvgR2,V1min,V1max);

	return s_count;
}

int main(int argc, char**argv)
{
	printf("\n==============================================================================");
	printf("\n=======================         Sampling101        ===========================");
	printf("\n==============================================================================");

	readfiles();

	s_count=0;
	border = sampling(1);
	/*s_count=0;
	sampling(2);*/

	printf("\nBoundary1 = %d", border);

	printf("\n\nPassing (%d) Samples to Renderer \n================================================\n",s_count);

	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_SAMPLES, 16);

	window = glfwCreateWindow(width, height, "3D Viewer", NULL, NULL);

	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouse);
	glfwSetCursorPosCallback(window, motion);
	glfwSetWindowSizeCallback(window, reshape);
	glfwSetScrollCallback(window, scroll);

	startGlew();

	while (!glfwWindowShouldClose(window))
	{
		draw();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}
