/*
 * Grid.h
 *
 *  Created on: Oct 30, 2017
 *      Author: fahimhkhan
 */

#ifndef GRID_H_
#define GRID_H_

#define BLOCK_SIZE 4

class BBox
{
public:
	float x;
	float y;
	float z;

	void setpoints(float x1, float y1, float z1){
			x=x1;
			y=y1;
			z=z1;
		}
};

class SamplePoint
{
public:
	float x;
	float y;
	float z;
	float u;
	float v;
	float w;
	int hLevel;
	glm::vec3 box[2][2][2];

	SamplePoint(){
		x=0.0;
		y=0.0;
		z=0.0;
		u=0.0;
		v=0.0;
		w=0.0;
		hLevel=0;
		for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				for (int k=0;k<2;k++){
					box[i][j][k] = glm::vec3(0.0,0.0,0.0);
					}
	}

	SamplePoint operator=(const SamplePoint  & rhs)
	{
	     if(this == &rhs)
	        return *this; // calls copy constructor SimpleCircle(*this)
	    x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		u = rhs.u;
		v = rhs.v;
		w = rhs.w;
		hLevel = rhs.hLevel;
		for (int i=0;i<2;i++)
				for (int j=0;j<2;j++)
							for (int k=0;k<2;k++){
								box[i][j][k]=rhs.box[i][j][k];
							}
	     return *this; // calls copy constructor
	}

	void setpoints(float x1, float y1, float z1, int L1){
		x=x1;
		y=y1;
		z=z1;
		hLevel=L1;
	}

	void setpoints(float x1, float y1, float z1, float u1, float v1, float w1, int L1){
		x=x1;
		y=y1;
		z=z1;
		u=u1;
		v=v1;
		w=w1;
		hLevel=L1;
	}

	void print(){
		printf("\n>>PRINTobj=(%f, %f, %f, %d)", x, y, z, hLevel);
	}

	void print2(){
			printf(">>PRINTobj=(%f, %f, %f, %f, %f, %f, %d)", x, y, z, u, v, w, hLevel);
		}

	void printBBox(){
		printf("\nBounding box");
			for (int i=0;i<2;i++)
				for (int j=0;j<2;j++)
					for (int k=0;k<2;k++){
						printf("<>[%f, %f, %f]", box[i][j][k].x, box[i][j][k].y, box[i][j][k].z);
						}
		}
};

class GridCell
{
public:
	SamplePoint P[2][2][2];
};

class Block
{
public:
	SamplePoint BP[BLOCK_SIZE][BLOCK_SIZE][BLOCK_SIZE];
};

#endif /* GRID_H_ */
