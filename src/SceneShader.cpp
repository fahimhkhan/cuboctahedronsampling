/*
 * SceneShader.cpp
 *
 */

#include "SceneShader.h"
#include "lodepng.h"
#include <string>

int xL,yL,zL,csp;
glm::vec3 Centered;

SceneShader::SceneShader(  shared_ptr<Tucano::Trackball> camera ): Shader()
{	
	_programMesh = 0;
	_programGlyph = 0;
	_programAxis = 0;
	_axisVertexArray = -1;
	_axisVertexArray = -1;
	_mvUniform = -1;
	_projUniform = -1;
	_zTranslation = 1.0;
	_aspectRatio = 1.0;
	//lightPosition = glm::vec3(0.5, 0.5, 0.5);
	lightPosition = glm::vec3(10.0, 10.0, 10.0);
	_camera = camera;
	GridFlag = 1;
	GlyphFlag = 2;
	LevelFlag = 1;
	TexFlag = 1;
	border = 0;
	arrowSize = 10;
}

void SceneShader::SetParameters(int H,int W,int D, int c, int b, SamplePoint SPP[6][1000000], float* dataP){
	xL = H;
	yL = W;
	zL = D;
	csp = c;
	border = b;
	dataV=dataP;
	for(int i=0;i<6;i++)
		for(int j=0;j<csp;j++)
			SPR[i][j] = SPP[i][j];
	Centered = glm::vec3(H/2, W/2, D/2);
	//Centered = glm::vec3(0.0,0.0, 0.0);
	printf("\nborder = %d", border);
}

void SceneShader::createVertexBuffer()
{

	GLfloat quadData[1000000], vData[1000000];

	for(int i=0;i<csp;i++){
		quadData[(i*3)]=SPR[1][i].x;
		quadData[(i*3)+1]=SPR[1][i].y;
		quadData[(i*3)+2]=SPR[1][i].z;
		vData[(i*3)]=SPR[1][i].u;
		vData[(i*3)+1]=SPR[1][i].v;
		vData[(i*3)+2]=SPR[1][i].w;
	}


	//passing model attributes to the GPU
	glGenVertexArrays(1, &_glyphVertexArray);
	glBindVertexArray(_glyphVertexArray);

	glGenBuffers(1, &_glyphVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _glyphVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof (quadData), quadData, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &_glyphDirectionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _glyphDirectionBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof (vData), vData, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	//Reading Texture
	unsigned int error;
	std::string imageFilename[10] = {"textures/blue.png",
                                    "textures/orange.png",
                                    "textures/blue2.png",
                                    "textures/arrow1.png",
                                    "textures/arrow2.png",
                                    "textures/blueArrow.png",
                                    "textures/grayForMagnitude.png",
                                    "textures/greenArrow.png",
                                    "textures/purplePastelArrow.png",
                                    "textures/blackStrokeB.png"};

    for(int i = 0; i < 10; i++){
        error = lodepng::decode( _image[i], _imageWidth, _imageHeight, imageFilename[i].c_str());
        _textureImageID[i] = _texture.create2DTexture(_image[i], _imageWidth, _imageHeight);
    }

    if(error) std::cout << "reading error "<<error<<":"<<lodepng_error_text(error) <<std::endl;

    //Reading Volume


    _VOLtextureImageID = _VOLtexture.create3DTexture( dataV, xL, yL, zL);

	return;
}


void SceneShader::startup()
{
	//	_camera->setPerspectiveMatrix(45.0, 1.0, 0.01f, 100.0f);
	//		_camera->setRenderFlag(true);
	//		_camera->setViewport(Eigen::Vector2f ((float)2048, (float)2048));
	//		_camera->increaseZoom(1.0);

	_programAxis = compile_shaders("./shaders/axis.vert", "./shaders/axis.frag");

	_programLight = compile_shaders("./shaders/light.vert", "./shaders/light.frag");

	_programGlyph = compile_shaders("./shaders/glyph.vert", "./shaders/glyph.frag", "./shaders/glyph.glsl");

	createVertexBuffer();


	_edgeDetectionRender = std::make_shared<EdgeDetectionRender>();
	_edgeDetectionRender->startup();

	_quadRender = std::make_shared<SimpleQuadRender>();
	_quadRender->startup();

	_fbo = std::make_shared<FrameBuffer>(1280, 1024, true, FrameBuffer::Type::ONE_ATTACHMENT);
	_fbo->createFBO32bits();
	_fbo->OFF();

	printInfo();
}

void SceneShader::renderSample(SamplePoint *PS, glm::vec3 color)
{
	for(int i=0;i<csp;i++){
		glm::vec3 Position = glm::vec3(PS[i].x, PS[i].y, PS[i].z);
			glUseProgram(_programLight);

			//uniform variables
			glUniformMatrix4fv(glGetUniformLocation(_programLight, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
			glUniformMatrix4fv(glGetUniformLocation(_programLight, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());


			glUniform3fv(glGetUniformLocation(_programLight, "lightPosition"), 1, glm::value_ptr(Position-Centered) );

			if(PS[i].hLevel==0){
				glPointSize(8.0f);
				}
			else{
				glPointSize(4.0f);
				}

			glUniform3fv(glGetUniformLocation(_programLight, "icolor"), 1, glm::value_ptr(color));

			if(PS[i].hLevel<(LevelFlag/3)){
				glDrawArrays( GL_POINTS, 0, 1);
			}
	}

}

void SceneShader::renderLight()
{
			glUseProgram(_programLight);

			//uniform variables
			glUniformMatrix4fv(glGetUniformLocation(_programLight, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
			glUniformMatrix4fv(glGetUniformLocation(_programLight, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());
			glUniform3fv(glGetUniformLocation(_programLight, "lightPosition"), 1, glm::value_ptr(lightPosition) );

			glUniform3fv(glGetUniformLocation(_programLight, "icolor"), 1, glm::value_ptr(glm::vec3(1.0, 0.5, 0.0)));

			glPointSize(25.0f);
			glDrawArrays(GL_POINTS, 0, 1);

}

void SceneShader::renderBBox(SamplePoint PB, int index)
{
	//create geometry
	GLfloat quadData[] =
	{
			PB.box[1][0][0].x, PB.box[1][0][0].y, PB.box[1][0][0].z,
			PB.box[0][0][0].x, PB.box[0][0][0].y, PB.box[0][0][0].z,
			PB.box[0][1][0].x, PB.box[0][1][0].y, PB.box[0][1][0].z,
			PB.box[1][1][0].x, PB.box[1][1][0].y, PB.box[1][1][0].z,
			PB.box[1][1][1].x, PB.box[1][1][1].y, PB.box[1][1][1].z,
			PB.box[1][0][1].x, PB.box[1][0][1].y, PB.box[1][0][1].z,
			PB.box[1][0][0].x, PB.box[1][0][0].y, PB.box[1][0][0].z,
			PB.box[1][1][0].x, PB.box[1][1][0].y, PB.box[1][1][0].z,
			PB.box[1][1][1].x, PB.box[1][1][1].y, PB.box[1][1][1].z,
			PB.box[0][1][1].x, PB.box[0][1][1].y, PB.box[0][1][1].z,
			PB.box[1][1][1].x, PB.box[1][1][1].y, PB.box[1][1][1].z,
			PB.box[1][0][1].x, PB.box[1][0][1].y, PB.box[1][0][1].z,
			PB.box[0][0][1].x, PB.box[0][0][1].y, PB.box[0][0][1].z,
			PB.box[0][0][0].x, PB.box[0][0][0].y, PB.box[0][0][0].z,
			PB.box[0][1][0].x, PB.box[0][1][0].y, PB.box[0][1][0].z,
			PB.box[0][1][1].x, PB.box[0][1][1].y, PB.box[0][1][1].z,
			PB.box[0][0][1].x, PB.box[0][0][1].y, PB.box[0][0][1].z,
	};

	//passing model attributes to the GPU
	//axis
	glGenVertexArrays(1, &_axisVertexArray);
	glBindVertexArray(_axisVertexArray);

	glGenBuffers(1, &_axisVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _axisVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof (quadData), quadData, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	glBindVertexArray(_axisVertexArray);

	glUseProgram(_programAxis);

	glUniformMatrix4fv(glGetUniformLocation(_programAxis, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
	glUniformMatrix4fv(glGetUniformLocation(_programAxis, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());

	glUniform3fv(glGetUniformLocation(_programAxis, "icolor"), 1, glm::value_ptr(glm::vec3(0.0, GridFlag/2.0 , 0.0)));
	glUniform3fv(glGetUniformLocation(_programAxis, "translate"), 1, glm::value_ptr(-Centered));

	glLineWidth(1.0f);
	glDrawArrays(GL_LINE_STRIP, 0, 17);


	//glLineWidth(1.0f);
	//glDrawArrays(GL_LINE_STRIP, 8, 14);

	glBindVertexArray(0);
}

void SceneShader::renderQuad(SamplePoint PB)
{
	//create geometry
		GLfloat w = 0.05;
		GLfloat x1 = PB.x;
		GLfloat y1 = PB.y;
		GLfloat z1 = PB.z;

		//GLfloat d =sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1))+((z2-z1)*(z2-z1)));
		GLfloat d =sqrt((PB.u*PB.u)+(PB.v*PB.v)+(PB.w*PB.w));
		GLfloat dm = 5*d;

		GLfloat x2 = x1 + (PB.u/dm);
		GLfloat y2 = y1 + (PB.v/dm);
		GLfloat z2 = z1 + (PB.w/dm);

		Eigen::Vector3f CV = _camera->getCenter();
		//glm::vec3 CV1 = glm::vec3(CV(0)-x1, CV(1)-y1, CV(2)-z1);
		glm::vec3 CV1 = glm::vec3(CV(0), CV(1), CV(2));

		//printf("\n1.Camera Vector %f,%f,%f", CV1.x, CV1.y, CV1.z);

		glm::vec3 D1 = glm::vec3(PB.u, PB.v, PB.w);

		glm::vec3 D2 = glm::normalize(glm::cross(D1,CV1));

		GLfloat x3 = x1 + (w*D2.x);
		GLfloat y3 = y1 + (w*D2.y);
		GLfloat z3 = z1 + (w*D2.z);

		GLfloat x4 = x2 + (w*D2.x);
		GLfloat y4 = y2 + (w*D2.y);
		GLfloat z4 = z2 + (w*D2.z);

		x1 = x1 - (w*D2.x);
		y1 = y1 - (w*D2.y);
		z1 = z1 - (w*D2.z);

		x2 = x2 - (w*D2.x);
		y2 = y2 - (w*D2.y);
		z2 = z2 - (w*D2.z);

		//printf("\nGlyphs %f,%f,%f to %f,%f,%f, dist=%f", x1, y1, z1, x2, y2, z2, d);
		//printf("\nPerpendicular Vector %f,%f,%f", x3, y3, z3);

		GLfloat quadData[] =
		{
				x1, y1, z1,
				x2, y2, z2,
				x3, y3, z3,
				x4, y4, z4,
		};

		//passing model attributes to the GPU
		//axis
		glGenVertexArrays(1, &_axisVertexArray);
		glBindVertexArray(_axisVertexArray);

		glGenBuffers(1, &_axisVertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, _axisVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof (quadData), quadData, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		glUseProgram(_programAxis);

		glUniformMatrix4fv(glGetUniformLocation(_programAxis, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
		glUniformMatrix4fv(glGetUniformLocation(_programAxis, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());

		glUniform3fv(glGetUniformLocation(_programAxis, "icolor"), 1, glm::value_ptr(glm::vec3(0.0, 1.0, 0.0)));
		glUniform3fv(glGetUniformLocation(_programAxis, "translate"), 1, glm::value_ptr(-Centered));

		//glLineWidth(2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		//glPointSize(7.0f);
		//glDrawArrays(GL_POINTS, 1, 1);
		glBindVertexArray(0);
}

void SceneShader::renderBBoxGeometry(SamplePoint *PB)
{
	//create geometry
	GLfloat geoData[1000000];

	for(int i=0;i<csp;i++){
		GLfloat quadData[] =
			{
					PB[i].box[1][0][0].x, PB[i].box[1][0][0].y, PB[i].box[1][0][0].z,
					PB[i].box[0][0][0].x, PB[i].box[0][0][0].y, PB[i].box[0][0][0].z,
					PB[i].box[0][1][0].x, PB[i].box[0][1][0].y, PB[i].box[0][1][0].z,
					PB[i].box[1][1][0].x, PB[i].box[1][1][0].y, PB[i].box[1][1][0].z,
					PB[i].box[1][1][1].x, PB[i].box[1][1][1].y, PB[i].box[1][1][1].z,
					PB[i].box[1][0][1].x, PB[i].box[1][0][1].y, PB[i].box[1][0][1].z,
					PB[i].box[1][0][0].x, PB[i].box[1][0][0].y, PB[i].box[1][0][0].z,
					PB[i].box[1][1][0].x, PB[i].box[1][1][0].y, PB[i].box[1][1][0].z,
					PB[i].box[1][1][1].x, PB[i].box[1][1][1].y, PB[i].box[1][1][1].z,
					PB[i].box[0][1][1].x, PB[i].box[0][1][1].y, PB[i].box[0][1][1].z,
					PB[i].box[1][1][1].x, PB[i].box[1][1][1].y, PB[i].box[1][1][1].z,
					PB[i].box[1][0][1].x, PB[i].box[1][0][1].y, PB[i].box[1][0][1].z,
					PB[i].box[0][0][1].x, PB[i].box[0][0][1].y, PB[i].box[0][0][1].z,
					PB[i].box[0][0][0].x, PB[i].box[0][0][0].y, PB[i].box[0][0][0].z,
					PB[i].box[0][1][0].x, PB[i].box[0][1][0].y, PB[i].box[0][1][0].z,
					PB[i].box[0][1][1].x, PB[i].box[0][1][1].y, PB[i].box[0][1][1].z,
					PB[i].box[0][0][1].x, PB[i].box[0][0][1].y, PB[i].box[0][0][1].z,
			};
		for(int j=0;j<51;j++){
			geoData[(i*51)+j] = quadData[j];
		}
	}


	//passing model attributes to the GPU
	//axis
	glGenVertexArrays(1, &_axisVertexArray);
	glBindVertexArray(_axisVertexArray);

	glGenBuffers(1, &_axisVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _axisVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof (geoData), geoData, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	glBindVertexArray(_axisVertexArray);

	glUseProgram(_programAxis);

	glUniformMatrix4fv(glGetUniformLocation(_programAxis, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
	glUniformMatrix4fv(glGetUniformLocation(_programAxis, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());

	glUniform3fv(glGetUniformLocation(_programAxis, "icolor"), 1, glm::value_ptr(glm::vec3(0.0, GridFlag/2.0 , 0.0)));
	glUniform3fv(glGetUniformLocation(_programAxis, "translate"), 1, glm::value_ptr(-Centered));

	for(int i=0;i<csp;i++){
		glLineWidth(1.0f);
		if(PB[i].hLevel==(int)(LevelFlag/3))
			glDrawArrays(GL_LINE_STRIP, (i*51), 17);
	}

	//glDrawArrays(GL_LINE_STRIP, 0, 17);

	glBindVertexArray(0);
}

void SceneShader::renderGlyphGeometry(SamplePoint *PBA, glm::vec3 color){

	//GLfloat *geoData = (GLfloat *) malloc((6*csp) * sizeof (GLfloat));
	GLfloat geoData[1000000];
	for(int i=0;i<csp;i++){
			GLfloat v[6];
			v[0] = PBA[i].x;
			v[1] = PBA[i].y;
			v[2] = PBA[i].z;

			GLfloat x2 = PBA[i].x + PBA[i].u;
			GLfloat y2 = PBA[i].y + PBA[i].v;
			GLfloat z2 = PBA[i].z + PBA[i].w;

			//GLfloat d =sqrt(((x2-v[0])*(x2-v[0]))+((y2-v[1])*(y2-v[1]))+((z2-v[2])*(z2-v[2])));
			GLfloat d =sqrt((PBA[i].u*PBA[i].u)+(PBA[i].v*PBA[i].v)+(PBA[i].w*PBA[i].w));

			int f = (PBA[i].hLevel==0) ? 3 : 1;
			GLfloat dm = 5*d/f;

			v[3] = PBA[i].x + (PBA[i].u/dm);
			v[4] = PBA[i].y + (PBA[i].v/dm);
			v[5] = PBA[i].z + (PBA[i].w/dm);

			//printf("\nGlyphs %f,%f,%f to %f,%f,%f, dist=%f", x1, y1, z1, x2, y2, z2, d);

			for (int j=0;j<6;j++){
				geoData[(i*6)+j]=v[j];
			}
	}

	//passing model attributes to the GPU
	glGenVertexArrays(1, &_axisVertexArray);
	glBindVertexArray(_axisVertexArray);

	glGenBuffers(1, &_axisVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _axisVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof (geoData), geoData, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	glBindVertexArray(_axisVertexArray);

	glUseProgram(_programAxis);

	glUniformMatrix4fv(glGetUniformLocation(_programAxis, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
	glUniformMatrix4fv(glGetUniformLocation(_programAxis, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());

	glUniform3fv(glGetUniformLocation(_programAxis, "icolor"), 1, glm::value_ptr(color));

	glUniform3fv(glGetUniformLocation(_programAxis, "translate"), 1, glm::value_ptr(-Centered));

	for(int i=0;i<csp;i++){
		if(PBA[i].hLevel<(LevelFlag/3)){

			int f = (PBA[i].hLevel==0) ? 3 : 1;

			glLineWidth(f*2.0f);
			glDrawArrays(GL_LINE_STRIP, (i*2), 2);
			glPointSize(f*4.0f);
			glDrawArrays(GL_POINTS, (1+(i*2)), 1);
		}
	}
	glBindVertexArray(0);
}

void SceneShader::renderGlyph(SamplePoint PB, int index)
{
	//create geometry

	int f=0;
	if (PB.hLevel==0)
		f=3;
	else
		f=1;

		GLfloat x1 = PB.x;
		GLfloat y1 = PB.y;
		GLfloat z1 = PB.z;

		GLfloat d =sqrt((PB.u*PB.u)+(PB.v*PB.v)+(PB.w*PB.w));
		GLfloat dm = (5*d)/f;

		GLfloat x2 = PB.x + (PB.u/dm);
		GLfloat y2 = PB.y + (PB.v/dm);
		GLfloat z2 = PB.z + (PB.w/dm);

		//printf("\nGlyphs %f,%f,%f to %f,%f,%f, dist=%f", x1, y1, z1, x2, y2, z2, d);

		GLfloat quadData[] =
		{
				x1, y1, z1,
				x2, y2, z2,
		};


		//passing model attributes to the GPU
		glGenVertexArrays(1, &_axisVertexArray);
		glBindVertexArray(_axisVertexArray);

		glGenBuffers(1, &_axisVertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, _axisVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof (quadData), quadData, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		glBindVertexArray(0);

		glBindVertexArray(_axisVertexArray);

		glUseProgram(_programAxis);

		glUniformMatrix4fv(glGetUniformLocation(_programAxis, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
		glUniformMatrix4fv(glGetUniformLocation(_programAxis, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());

		if(index<border)
			glUniform3fv(glGetUniformLocation(_programAxis, "icolor"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 1.0)));
		else
			glUniform3fv(glGetUniformLocation(_programAxis, "icolor"), 1, glm::value_ptr(glm::vec3(1.0, 0.5, 0.0)));

		glUniform3fv(glGetUniformLocation(_programAxis, "translate"), 1, glm::value_ptr(-Centered));


		glLineWidth(f*2.0f);
		glDrawArrays(GL_LINE_STRIP, 0, 2);
		glPointSize(f*4.0f);
		glDrawArrays(GL_POINTS, 1, 1);
		glBindVertexArray(0);
}

void SceneShader::renderGlyphTex(SamplePoint *PB)
{
	glBindVertexArray(_glyphVertexArray);

	glUseProgram(_programGlyph);

	_texture.bind2DTexture(_programGlyph, _textureImageID[(int)(TexFlag/3)], std::string("image"));

	_VOLtexture.bind3DTexture(_programGlyph, _VOLtextureImageID, std::string("VOLimage"));

	glUniformMatrix4fv(glGetUniformLocation(_programGlyph, "modelview"), 1, GL_FALSE,  _camera->getViewMatrix().data());
	glUniformMatrix4fv(glGetUniformLocation(_programGlyph, "projection"), 1, GL_FALSE, _camera->getProjectionMatrix().data());

	glUniform3fv(glGetUniformLocation(_programGlyph, "icolor"), 1, glm::value_ptr(glm::vec3(1.0, 0.5, 0.0)));
	glUniform3fv(glGetUniformLocation(_programGlyph, "VolSize"), 1, glm::value_ptr(glm::vec3(xL, yL, zL)));
	glUniform3fv(glGetUniformLocation(_programGlyph, "translate"), 1, glm::value_ptr(-Centered));
	glUniform1f(glGetUniformLocation(_programGlyph, "arrowSize"), arrowSize);

	//glPointSize(6.0f);

	for(int i=0;i<csp;i++){
		if(PB[i].hLevel == 0){
			glUniform1i(glGetUniformLocation(_programGlyph, "repeat"), 8);
			glDrawArrays(GL_POINTS, i, 1);
		}
		else{
			glUniform1i(glGetUniformLocation(_programGlyph, "repeat"), 4);
			glDrawArrays(GL_POINTS, i, 1);
		}
	}

    //glDrawArrays(GL_POINTS, 0, csp);
	glBindVertexArray(0);
}

void SceneShader::render()
{
	//Render Grid
	GLfloat color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat one = 1.0f;

	glClearBufferfv(GL_COLOR, 0, color);
	glClearBufferfv(GL_DEPTH, 0, &one);

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glEnable(GL_DEPTH_TEST);

	//Render light
	//renderLight();

	_fbo->ON( 1280, 1024);
			glClearBufferfv(GL_COLOR, 0, color);
			glClearBufferfv(GL_COLOR, 1, color);
			glClearBufferfv(GL_COLOR, 2, color);
			glClearBufferfv(GL_DEPTH, 0, &one);

			glEnable(GL_DEPTH_TEST);

			if(GridFlag>0){
				SamplePoint M;
				for (int i=0;i<2;i++)
					for (int j=0;j<2;j++)
						for (int k=0;k<2;k++){
							M.box[i][j][k] = glm::vec3(i*(xL-1),j*(yL-1),k*(zL-1));
						}
				renderBBox(M,0);
			}
		//
		//	//Render Sample
			if(GlyphFlag==0){
				renderSample(SPR[1], glm::vec3(0.0, 0.0, 1.0));
		//		renderSample(SPR[2], glm::vec3(1.0, 0.5, 0.0));
			}


			if(GlyphFlag==1){
				renderGlyphGeometry(SPR[1], glm::vec3(0.0, 0.0, 1.0));
		//		//renderGlyphGeometry(SPR[2], glm::vec3(1.0, 0.5, 0.0));
			}
			//if(GridFlag==2) renderBBoxGeometry(SPR[1]);
			if(GlyphFlag==2) renderGlyphTex(SPR[1]);

			for(int i=0;i<csp;i++){
				//if(GlyphFlag==2) renderGlyph1(SPR[1][i]);
				//if(GlyphFlag==1) renderQuad(SPR[1][i]);
				if((GridFlag==2)&&(SPR[1][i].hLevel==(int)(LevelFlag/3))) renderBBox(SPR[1][i], i);
				//if((GridFlag==2)&&(SPR[1][i].hLevel>2)) renderBBox(SPR[1][i], i);
			}


			if(GlyphFlag==2) renderGlyphTex(SPR[1]);

			glDisable(GL_DEPTH_TEST);
	_fbo->OFF();

	//glDisable(GL_DEPTH_TEST);
	//glDisable(GL_BLEND);

	_quadRender->setTexture(_fbo->_attach0);
	_quadRender->render();
}

void SceneShader::printInfo()
{
	//system("cls");
	printf("\n==========================================================================================");
	printf("\n=============================    RENDERING ..........   ==================================");
	printf("\n==========================================================================================\n");
	//printf("\n\tControl Camera using Left and Right Mouse buttons");
	printf("\n Level = %d",(int)(LevelFlag/3));
}

void SceneShader::GridToggle(int s)
{
	GridFlag=s;
}

void SceneShader::GlyphToggle(int s)
{
	GlyphFlag=s;
}

void SceneShader::LevelToggle(int s)
{
	LevelFlag = LevelFlag+s;
	printInfo();
}

void SceneShader::TexToggle(int s)
{
	TexFlag = TexFlag+s;
	//printInfo();
}

void SceneShader::ArrowSize(int s)
{
	arrowSize = arrowSize+s;
	//printInfo();
}

void SceneShader::shutdown()
{
	glDeleteBuffers(1, &_meshVertexBuffer);
	glDeleteBuffers(1, &_meshNormalBuffer);
	glDeleteBuffers(1, &_meshIndicesBuffer);
	glDeleteVertexArrays(1, &_meshVertexArray);
}

SceneShader::~SceneShader()
{
	shutdown();
}
