#ifndef EDGEDETECTIONRENDER_H
#define EDGEDETECTIONRENDER_H

#include "Shader.h"
#include <glm/glm.hpp>
#include <vector>


class EdgeDetectionRender : public Shader
{
public:

    EdgeDetectionRender();

    void startup();
    void shutdown ();
    void render();

    void setTexture( GLuint texture );

    ~EdgeDetectionRender();

protected:

    void createVertexBuffer();

	GLuint _program;
	GLuint _vertexArray;
	GLuint _vertexBuffer;
	GLuint _textureID;
};

#endif // EDGEDETECTIONRENDER_H
