#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "GL/glew.h"

class FrameBuffer
{

public:
    enum class Type{ONE_ATTACHMENT, TWO_ATTACHMENT, THREE_ATTACHEMENT, LAYERED};
    FrameBuffer( unsigned int w, unsigned int h, bool isLinearFilter, Type type );

    void createLayeredFramebuffer(int nLayers);
    void createFBO32bits();
    void createFBO32bitsOneAttachment();
    void createMultisampleFramebuffer();
    void ON( int width, int height );
    void ON(int originX, int originY, int width, int height);
    void OFF();
    GLuint getColorBuffer(){ return _attach0; }
    GLuint getDepthBuffer(){ return _depthAttach; }
    void checkFramebufferStatus();
    ~FrameBuffer();

    GLuint _attach0;
    GLuint _attach1;
    GLuint _attach2;
    GLuint _attach3;
    GLuint _attach4;
    GLuint _depthAttach;
    GLuint _fbo;

    void getDimensions( int& w, int& h);


private:

    int _width;
    int _height;
    bool _isLinearFilter;

    Type _type;



};

#endif // FRAMEBUFFER_H
