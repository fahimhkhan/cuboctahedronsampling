#include "framebuffer.h"
#include <iostream>

FrameBuffer::FrameBuffer( unsigned int w, unsigned int h, bool isLinear, Type type )
{
	_width = w;
	_height = h;
	_isLinearFilter = isLinear;
	_type = type;
}


void FrameBuffer::checkFramebufferStatus()
{
	GLenum fboStatus = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer was not created correctly." << std::endl;

	if(fboStatus)
	{
		switch (fboStatus) {
		case GL_FRAMEBUFFER_UNDEFINED:
			std::cout << "Opps, no window exists?" << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			std::cout << "Check the status of each attachement"<< std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			std::cout << "Attach at least one buffer to the FBO" << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			std::cout << "Check that all attachements enabled via glDrawBuffers exist in FBO " << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			std::cout << "Check that the buffer specified via glReadBuffer exists in FBO" << std::endl;
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			std::cout << "Reconsider formats used for attached buffers" << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			std::cout << "Make sure the number of samples for each attachment is the same" << std::endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
			std::cout << "Make sure the number of layers for each attachment if the same" << std::endl;
			break;
		default:
			break;
		}
	}
}


void FrameBuffer::createLayeredFramebuffer(int nLayers)
{

	// std::cout << "Number of Layers in the FBO: "<< nLayers << std::endl;
	// Create a frame buffer object and bind it
	glGenFramebuffers(1, &_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

	//Create a texture for our color buffer
	glGenTextures(1, &_attach0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _attach0);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA32F, _width, _height, nLayers);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST);


	// Create a texture that will be our FBO's depth buffer
	glGenTextures(1, &_depthAttach);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _depthAttach);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT24, _width, _height, nLayers);


	// New, attach the color and depth textures to the FBO
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _attach0, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthAttach, 0);


	// Tell OPENGL that we want to draw into the frame buffer's color attachment
	static const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, draw_buffers);

	// Always check that our framebuffer is ok
	//if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	checkFramebufferStatus();

	//    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void FrameBuffer::createFBO32bitsOneAttachment()
{
	// Create a frame buffer object and bind it
	glGenFramebuffers(1, &_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

	//Create a texture for our color buffer
	glGenTextures(1, &_attach0);
	glBindTexture(GL_TEXTURE_2D, _attach0);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	// Create a texture that will be our FBO's depth buffer
	glGenTextures(1, &_depthAttach);
	glBindTexture(GL_TEXTURE_2D, _depthAttach);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, _width, _height);

	//    glGenTextures(1, &_attach2);
	//    glBindTexture(GL_TEXTURE_2D, _attach2);
	//    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	// New, attach the color and depth textures to the FBO
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _attach0, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthAttach, 0);

	// Tell OPENGL that we want to draw into the frame buffer's color attachment
	static const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, draw_buffers);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer was not created correctly." << std::endl;

	//glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::createMultisampleFramebuffer()
{
	// Create a frame buffer object and bind it
	glGenFramebuffers(1, &_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

	//Create a texture for our color buffer
	glGenTextures(1, &_attach0);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, _attach0);
	glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, GL_RGBA8, _width, _height, GL_TRUE);

	// Create a texture that will be our FBO's depth buffer
	glGenTextures(1, &_depthAttach);
	glBindTexture(GL_TEXTURE_2D, _depthAttach);
	glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, GL_DEPTH_COMPONENT, _width, _height, GL_TRUE);

	// New, attach the color and depth textures to the FBO
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _attach0, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthAttach, 0);

	// Tell OPENGL that we want to draw into the frame buffer's color attachment
	static const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, draw_buffers);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer was not created correctly." << std::endl;

	//glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void FrameBuffer::createFBO32bits( )
{
	// Create a frame buffer object and bind it
	glGenFramebuffers(1, &_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

	//Create a texture for our color buffer
	glGenTextures(1, &_attach0);
	glBindTexture(GL_TEXTURE_2D, _attach0);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	glGenTextures(1, &_attach1);
	glBindTexture(GL_TEXTURE_2D, _attach1);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	glGenTextures(1, &_attach2);
	glBindTexture(GL_TEXTURE_2D, _attach2);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	glGenTextures(1, &_attach3);
	glBindTexture(GL_TEXTURE_2D, _attach3);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	glGenTextures(1, &_attach4);
	glBindTexture(GL_TEXTURE_2D, _attach4);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, _width, _height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);

	// Create a texture that will be our FBO's depth buffer
	glGenTextures(1, &_depthAttach);
	glBindTexture(GL_TEXTURE_2D, _depthAttach);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, _width, _height);

	// New, attach the color and depth textures to the FBO
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _attach0, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, _attach1, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, _attach2, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, _attach3, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, _attach4, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthAttach, 0);

	// Tell OPENGL that we want to draw into the frame buffer's color attachment
	static const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4};
	glDrawBuffers(5, draw_buffers);

	// Always check that our framebuffer is ok
	checkFramebufferStatus();

	//    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	//        std::cout << "Framebuffer was not created correctly." << std::endl;
	// glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void FrameBuffer::ON( int width, int height )
{
	_width = width;
	_height = height;
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
	glViewport(0, 0, width, height); // Render on the whole frame buffer, complete from the lower left corner to the upper right


}

void FrameBuffer::ON(int originX, int originY, int width, int height)
{
	_width = (width - originX);
	_height = (height - originY);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
	glViewport(originX, originY, width, height); // Render on the whole frame buffer, complete from the lower left corner to the upper right
}

void FrameBuffer::OFF( )
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


FrameBuffer::~FrameBuffer()
{    
	switch (_type) {
	case Type::ONE_ATTACHMENT:
		glDeleteTextures(1, &_attach0);
		glDeleteTextures(1, &_depthAttach);
		break;
	case Type::TWO_ATTACHMENT:
		glDeleteTextures(1, &_attach0);
		glDeleteTextures(1, &_attach1);
		glDeleteTextures(1, &_depthAttach);
		break;
	case Type::THREE_ATTACHEMENT:
		glDeleteTextures(1, &_attach0);
		glDeleteTextures(1, &_attach1);
		glDeleteTextures(1, &_attach3);
		glDeleteTextures(1, &_depthAttach);
		break;
	case Type::LAYERED:
		glDeleteTextures(1, &_attach0);
		glDeleteTextures(1, &_depthAttach);
		break;
	default:
		break;
	}

	glDeleteFramebuffers(1, &_fbo);
}

void FrameBuffer::getDimensions( int &w,  int &h)
{
	w = _width;
	h = _height;
}
