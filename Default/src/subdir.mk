################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/SceneShader.cpp \
../src/Shader.cpp \
../src/edgedetectionrender.cpp \
../src/framebuffer.cpp \
../src/lodepng.cpp \
../src/main.cpp \
../src/simplequadrender.cpp \
../src/texture.cpp 

OBJS += \
./src/SceneShader.o \
./src/Shader.o \
./src/edgedetectionrender.o \
./src/framebuffer.o \
./src/lodepng.o \
./src/main.o \
./src/simplequadrender.o \
./src/texture.o 

CPP_DEPS += \
./src/SceneShader.d \
./src/Shader.d \
./src/edgedetectionrender.d \
./src/framebuffer.d \
./src/lodepng.d \
./src/main.d \
./src/simplequadrender.d \
./src/texture.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I"/home/fahimhkhan/cuda-workspace/CuboctahedronSampling/libraries/glew-2.1.0/include" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


