#version 450 core

out vec4 color;

uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

uniform sampler2D meshTexture;

in vec2 uv;

//Sobel filter
uniform int radius = 5;
// rendered image width
uniform float renderwidth = 2.0*1280;

uniform vec4 lineColor = vec4(0.0, 0.0, 0.0, 1.0);

uniform float boundaryThickness = 0.45;

mat3 sx = mat3(
    1.0, 2.0, 1.0,
    0.0, 0.0, 0.0,
   -1.0, -2.0, -1.0
);
mat3 sy = mat3(
    1.0, 0.0, -1.0,
    2.0, 0.0, -2.0,
    1.0, 0.0, -1.0
);


float computeLinearDepth( float depth )
{
    float zFar = 100.0; //dragon and aneurysm
    float zNear = 0.01; //dragon and aneurysm 0.01

    float z_n = 2.0 * depth - 1.0;

    float z_e = zNear * zFar / (zFar + zNear - z_n * (zFar - zNear));

    return z_e;

}

float intensity( in vec4 colorTemp )
{
    colorTemp.x = computeLinearDepth(colorTemp.w);
    colorTemp.y = computeLinearDepth(colorTemp.w);
    colorTemp.z = computeLinearDepth(colorTemp.w);

    return sqrt((colorTemp.x * colorTemp.x)+(colorTemp.y * colorTemp.y)+(colorTemp.z * colorTemp.z));
}



vec4 simple_edge_detection( in float step, in vec2 center )
{

    // let's learn more about our center pixel
    float center_intensity = intensity(texture(meshTexture, center));
    // counters we need
    int darker_count = 0;
    float max_intensity = center_intensity;
    // let's look at our neighboring points
    for (int i = -radius; i <= radius; i++)
    {
        for (int j = -radius; j <= radius; j++)
        {
            vec2 current_location = center + vec2(i*step, j * step);



            float current_intensity = intensity(texture(meshTexture, current_location));
            if (current_intensity < center_intensity)
            {
                darker_count++;
            }
            if (current_intensity > max_intensity)
            {
                max_intensity = current_intensity;
            }
        }
    }
    // do we have a valley pixel?
    if ((max_intensity - center_intensity)  > 0.01 * radius)
    {
        if (darker_count / (radius * radius) < (1.0 - (1.0 / radius) ))
        {
            return vec4(lineColor); // yep, it's a valley pixel.
        }

    }

    return vec4(1.0, 1.0, 1.0, 0.0);
}

void main()
{


vec2 center_color = gl_FragCoord.xy / textureSize(meshTexture, 0);
    vec3 diffuse = vec3(computeLinearDepth(texture(meshTexture, center_color).w));
       mat3 I;
       for (int i=0; i<3; i++) {
           for (int j=0; j<3; j++) {
               float intensity = computeLinearDepth(texelFetch(meshTexture, ivec2(gl_FragCoord) + ivec2(i-1,j-1), 0 ).w);
               vec3 sampler  = vec3(intensity);
               I[i][j] = length(sampler);
       }
   }

   float gx = dot(sx[0], I[0]) + dot(sx[1], I[1]) + dot(sx[2], I[2]);
   float gy = dot(sy[0], I[0]) + dot(sy[1], I[1]) + dot(sy[2], I[2]);

   float g = sqrt(pow(gx, 2.0)+ pow(gy, 2.0));

   //if((1.0 - g) < 0.85) //aneurysm and dragon
   if((1.0 - g) < 0.85)

        color = vec4(vec3(0.0),  1.0);
   else
        color = vec4(1.0, 1.0, 1.0, 0.0);
        
        
	//color = vec4(texture(meshTexture, center_color).xyz, 1.0);


//    //* Edge Detection
//    float step = boundaryThickness/renderwidth;
//    vec2 center_color = gl_FragCoord.xy / textureSize(meshTexture, 0);
//    color = simple_edge_detection(step, center_color);

//    if( color.x == 1.0)
//         discard;

}
