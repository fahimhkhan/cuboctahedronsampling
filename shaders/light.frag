#version 430 core

out vec4 color;

uniform vec3 icolor;

void main (void)
{
	color = vec4( icolor, 1.0);
}