#version 430 core

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec3 position;

out vec3 N;
out vec3 P;
out vec3 L;

uniform vec3 lightPosition;
uniform vec3 translate;

void main (void)
{	
	vec4 P4 = modelview * vec4(position + translate, 1.0);
    P = P4.xyz/P4.w;

    gl_Position = projection *  P4;
}
