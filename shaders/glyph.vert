#version 450 core

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 dir;

out vec3 P;
out vec3 N;

out float distToCamera;

uniform vec3 translate;

void main (void)
{	
	vec4 P4 = vec4(vertex + translate, 1.0);
    //P = P4.xyz/P4.w;
    P = P4.xyz;
	
	N = dir;
 
    //gl_Position = projection * P4;
    gl_Position = P4;
}
