#version 450 core

layout(points) in;
layout (triangle_strip, max_vertices = 96) out; //24 //48
//layout (line_strip, max_vertices = 96) out; //24 //48

in vec3 P[];
in vec3 N[];

uniform sampler3D VOLimage;

uniform mat4 modelview;
uniform mat4 projection;

uniform vec3 translate;
uniform vec3 VolSize;
uniform float arrowSize;
uniform int repeat;

out vec2 uv;
out vec3 pointout;
out vec3 Normal;

vec3 ExtractCameraPos()
{
  // Get the 3 basis vector planes at the camera origin and transform them into model space.
  //  
  // NOTE: Planes have to be transformed by the inverse transpose of a matrix
  //       Nice reference here: http://www.opengl.org/discussion_boards/showthread.php/159564-Clever-way-to-transform-plane-by-matrix
  //
  //       So for a transform to model space we need to do:
  //            inverse(transpose(inverse(MV)))
  //       This equals : transpose(MV) - see Lemma 5 in http://mathrefresher.blogspot.com.au/2007/06/transpose-of-matrix.html
  //
  // As each plane is simply (1,0,0,0), (0,1,0,0), (0,0,1,0) we can pull the data directly from the transpose matrix.
  //  
  mat4 modelViewT = transpose(modelview);
 
  // Get plane normals 
  vec3 n1 = vec3(modelViewT[0]);
  vec3 n2 = vec3(modelViewT[1]);
  vec3 n3 = vec3(modelViewT[2]);
 
  // Get plane distances
  float d1 = (modelViewT[0].w);
  float d2 = (modelViewT[1].w);
  float d3 = (modelViewT[2].w);
 
  // Get the intersection of these 3 planes
  // http://paulbourke.net/geometry/3planes/
  vec3 n2n3 = cross(n2, n3);
  vec3 n3n1 = cross(n3, n1);
  vec3 n1n2 = cross(n1, n2);
 
  vec3 top = (n2n3 * d1) + (n3n1 * d2) + (n1n2 * d3);
  float denom = dot(n1, n2n3);
 
  return top / -denom;
}


void main()
{
	  vec3 point;
	  vec3 dir = N[0];    

	  //float txc=repeat/l;
	  int l=repeat;
	  float txc=0.5;
	  
	  //FORWARD trace
	  
	  point = P[0];
	  //for(int i=0;i<l;i++){  
	  for(int i=l;i<(2*l);i++){ 	  
	  	//float dm=400*length(normalMatrix*dir);
	  	//float dm=sqrt((dir.x*dir.x)+(dir.y*dir.y)+(dir.z*dir.z));
	  	vec3 uvw = (point-translate);
	  	uvw = vec3((uvw.x/VolSize.x),(uvw.y/VolSize.y),(uvw.z/VolSize.z));
	  	vec4 dirN = texture( VOLimage, uvw.yxz);
      	dir = dirN.xyz;
     	
      //Calculating vectors
      	mat3 normalMatrix = transpose(inverse(mat3(modelview)));
	  	vec3 D1 = normalize(dir); 
	    
	    vec3 eye = ExtractCameraPos();
	    
	    vec3 dm = vec3(1.0/(arrowSize));
	  	
	  	vec3 eyeDir = normalize(eye - point);		  		  
	  	
	  	vec3 D2 = normalize(cross(eyeDir, D1)); 
	  	
	  	//vec3 newVector = normalize(cross(eyeDir, D2));	
	  	
	  	vec3 P1 = point + D1*dm;

	//Drawing points
		uv = vec2(i*txc,0.0);	  	
	  	gl_Position = projection*modelview*vec4(point - D2*dm, 1.0);// First vertex of the Quad 
	  	Normal =   normalize(cross(D1, D2));   	
	  	//Normal =   normalMatrix*Normal;	
      	pointout = point;
      	EmitVertex();
        
        uv = vec2(i*txc,1.0);            
      	gl_Position = projection*modelview*vec4(point + D2*dm, 1.0);// Second vertex of the Quad  
      	Normal =   normalize(cross(D1, D2));  
      	//Normal =   normalMatrix*Normal;   	
      	pointout = point;
      	EmitVertex();      
 	 
 	 	uv = vec2((i+1)*txc,0.0);
	  	gl_Position = projection*modelview*vec4(P1 - D2*dm, 1.0);// Third vertex of the Quad	
	  	Normal =   normalize(cross(D1, D2)); 
	  	//Normal =   normalMatrix*Normal;  	
	  	pointout = point;
	  	EmitVertex();	  
     
     	uv = vec2((i+1)*txc,1.0);
      	gl_Position = projection*modelview*vec4(P1 + D2*dm, 1.0);// Fourth vertex of the Quad   
      	Normal =   normalize(cross(D1, D2));    
      	//Normal =   normalMatrix*Normal;	
      	pointout = point;
      	EmitVertex();
  
      	point = P1; 	

	  }
	  EndPrimitive();


	  //BACKWARD trace
	  point = P[0];
	  for(int i=l;i>0;i--){  	  
	  	//float dm=400*length(normalMatrix*dir);
	  	//float dm=sqrt((dir.x*dir.x)+(dir.y*dir.y)+(dir.z*dir.z));
	  	vec3 uvw = (point-translate);
	  	uvw = vec3((uvw.x/VolSize.x),(uvw.y/VolSize.y),(uvw.z/VolSize.z));
	  	vec4 dirN = texture( VOLimage, uvw.yxz);
      	dir = dirN.xyz;
     	
      //Calculating vectors
      	mat3 normalMatrix = transpose(inverse(mat3(modelview)));
	  	vec3 D1 = normalize(dir); 
	    
	    vec3 eye = ExtractCameraPos();
	    
	    vec3 dm = vec3(1.0/(arrowSize));
	  	
	  	vec3 eyeDir = normalize(eye - point);		  		  
	  	
	  	vec3 D2 = normalize(cross(eyeDir, D1)); 
	  	
	  	//vec3 newVector = normalize(cross(eyeDir, D2));	
	  	
	  	vec3 P1 = point - D1*dm;

	//Drawing points
		uv = vec2(i*txc,0.0);	  	
	  	gl_Position = projection*modelview*vec4(point - D2*dm, 1.0);// First vertex of the Quad 
	  	Normal =   normalize(cross(D1, D2)); 
	  	//Normal =   normalMatrix*Normal;	
      	pointout = point;
      	EmitVertex();
      	
      	uv = vec2((i-1)*txc,0.0);
	  	gl_Position = projection*modelview*vec4(P1 - D2*dm, 1.0);// Third vertex of the Quad	
	  	Normal =   normalize(cross(D1, D2));   	
	  	//Normal =   normalMatrix*Normal;	
	  	pointout = point;
	  	EmitVertex();
        
        uv = vec2(i*txc,1.0);            
      	gl_Position = projection*modelview*vec4(point + D2*dm, 1.0);// Second vertex of the Quad  
      	Normal =   normalize(cross(D1, D2)); 
      	//Normal =   normalMatrix*Normal;	    	
      	pointout = point;
      	EmitVertex();        
     
     	uv = vec2((i-1)*txc,1.0);
      	gl_Position = projection*modelview*vec4(P1 + D2*dm, 1.0);// Fourth vertex of the Quad   
      	Normal =   normalize(cross(D1, D2));
      	//Normal =   normalMatrix*Normal;	    	
      	pointout = point;
      	EmitVertex();
  
      	point = P1; 	

	  }
	  EndPrimitive();
}