#version 450 core

layout (location = 0) in vec4 vertex;

uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

out vec2 uv;

void main(void)
{
    uv = vertex.zw;

    vec3 position = vec3(vertex.xy, 0.0);
    gl_Position = vec4(position, 1.0);
}
