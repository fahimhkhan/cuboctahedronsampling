#version 450 core
//#extension GL_ARB_fragment_shader_interlock : enable
//#define GL_ARB_fragment_shader_interlock           1

in vec2 uv;
in vec3 pointout;
in vec3 Normal;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 normal;

uniform vec3 icolor;
uniform sampler2D image;

uniform vec3 translate;

float computeLinearDepth( float depth )
{
    float zFar = 10.0;
    float zNear = 0.01;
   	float z_n = 2.0 * depth - 1.0;
   	float z_e = zNear * zFar / (zFar + zNear - z_n * (zFar - zNear));
   	return z_e;
}

float normalizeDepth( float depth )
{
    float zFar = 100.0;
    float zNear = 0.01;
   	float z_e = (depth - zNear)/(zFar - zNear);
   	return z_e;
}

void main (void)
{
	
	//beginInvocationInterlockARB();
	vec4 colorImage = texture( image, uv);
	
	//color = vec4(uv, 0.0, 1.0);
	
	if( colorImage.w <= 0.2)
		discard;
		
	//float depth = normalizeDepth(gl_FragCoord.z);	
	//if ( depth > 0.45)
	//	depth = 5*(depth-0.45);
	
	float depth = computeLinearDepth(gl_FragCoord.z);
	float kd = 0.1;
	float attenuation = 0.3 + 1.0/(depth*depth);	
	
	//color = vec4((clamp(attenuation,0.0,1.0)*colorImage.xyz), 1.0);
	//color = vec4((colorImage.xyz), 1.0);
	color = vec4((clamp(attenuation,0.0,1.0)*colorImage.xyz), colorImage.w);
	//color = vec4(clamp(vec3(depth) + colorImage.xyz, 0, 1), 1.0);
	//color = vec4(Normal.x, Normal.y, Normal.z, 1.0);
	
	normal = vec4(Normal,depth);
	//endInvocationInterlockARB();
}
